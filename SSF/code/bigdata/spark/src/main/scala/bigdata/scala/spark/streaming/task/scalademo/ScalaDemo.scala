package bigdata.scala.spark.streaming.task.testscalaapp

import bigdata.java.framework.spark.kafka.{SKProcessor, SimpleSparkKafka}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.streaming.api.java.JavaInputDStream

object ScalaDemo {
  def main(args: Array[String]): Unit = {
    //在idea中调试时，Program arguments 参数
    //appname:FindDataStreaming topics:yxgk.a_Cashchk_Flow duration:2 maxnum:2000
    //appname=应用程序名称
    //topics=消费的kafka主题，多个主题用逗号隔开
    //duration=每隔多少秒执行一个spark批次
    //maxnum=每个批次获取多少数据，每隔批次消费的总数据量等于分区数量*秒数*maxnum
    val simpleSparkKafka = new SimpleSparkKafka(args)
    simpleSparkKafka.execute(new SKProcessor {
      override def process(directStream: JavaInputDStream[ConsumerRecord[String, String]]): Unit = {
        val stream = directStream.inputDStream
        stream.foreachRDD{rdd=>
          rdd.foreachPartition { partitionOfRecods =>
            partitionOfRecods.foreach(println)
          }
        }
      }
    }).start()
  }
}
