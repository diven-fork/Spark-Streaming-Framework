/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.ConfigUtil;
import bigdata.java.framework.spark.util.InputParameterUtil;
import bigdata.java.framework.spark.util.MySqlUtil;
import bigdata.java.framework.spark.util.StreamingUtil;
import com.typesafe.config.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import java.util.Map;


public class SimpleSparkKafka {

    String checkPoint="";

    public String getCheckPoint() {
        return checkPoint;
    }

    public void setCheckPoint(String checkPoint) {
        this.checkPoint = checkPoint;
    }

    SKContext skContext;

    public SimpleSparkKafka(String[] args)  throws InterruptedException
    {
        this(args,0);
    }

    public SimpleSparkKafka(String[] args, Integer length) throws InterruptedException {

        Boolean hasLocal = ConfigUtil.getConfig().hasPath("sparkconf.spark.master");
        String local = "";
        if(hasLocal)
        {
            local = ConfigUtil.getConfig().getString("sparkconf.spark.master");
        }

        if( hasLocal &&  !StringUtils.isBlank(local))
        {//本地ide环境运行
            String[] addArgs = new String[args.length + 1];
            String groupId = "";
            boolean hasGroupId = false;
            for (int i = 0; i < args.length; i++) {
                if(args[i].contains("appname:"))
                {
                    groupId = args[i].replace("appname:","groupid:");
                }
                if(args[i].contains("groupid:"))
                {
                    hasGroupId=true;
                }
            }
            if(!hasGroupId)
            {//没有包含groupid
                for (int i = 0; i < args.length; i++) {
                    addArgs[i] = args[i];
                }
                addArgs[args.length] = groupId;

                if(length !=0)
                {
                    length = length + 1;
                }
                args = addArgs;
            }
        }

        InputParameterUtil inputParameterUtil = new InputParameterUtil(args,length);
        SparkConf sparkConf = StreamingUtil.createGeneralConf(inputParameterUtil.getAppName());
        StreamingUtil.setBackPressure(sparkConf,inputParameterUtil.getMaxNum());
        Map<String, Object> kafkaParams = KafkaTools.createGeneralParams();
        kafkaParams.put(KafkaTools.GROUPID,inputParameterUtil.getGroupId());
        skContext = new SKContext();
        skContext.setInputParameterUtil(inputParameterUtil);
        skContext.setKafkaParams(kafkaParams);
        skContext.setSparkConf(sparkConf);
    }

    public SimpleSparkKafka execute(SKProcessor processor)
    {
        processor.setSkContext(skContext);
        InputParameterUtil inputParameterUtil = skContext.getInputParameterUtil();
        SparkConf sparkConf = skContext.getSparkConf();
        Map<String, Object> kafkaParams = skContext.getKafkaParams();

        String[] args = inputParameterUtil.getArgs();
        String[] kafkArgs = inputParameterUtil.getPrevList("kafkaparam.", args);
        String[] allKey = InputParameterUtil.getAllKey(kafkArgs);
        for (int i = 0; i < allKey.length; i++) {
            String key = allKey[i];
            String value = InputParameterUtil.getValue(key, kafkArgs);
            kafkaParams.put(key.replace("kafkaparam.",""),value);
        }

        processor.init(inputParameterUtil,sparkConf,kafkaParams);
        JavaStreamingContext jsc = new JavaStreamingContext(sparkConf, inputParameterUtil.getSeconds());
        if(inputParameterUtil.getIsCheckPoint())
        {
            boolean checkpoint = ConfigUtil.getConfig().getIsNull("checkpoint");
            if(checkpoint)
            {
                throw new RuntimeException("application.properties not find checkpoint parameter");
            }
            String checkpointDir = ConfigUtil.getConfig().getString("checkpoint") + "/" + inputParameterUtil.getAppName();
            jsc.checkpoint(checkpointDir);
            setCheckPoint(checkpointDir);
        }
        //设置日志级别
//        jsc.sparkContext().setLogLevel(StreamingUtil.LOGLEVEL);
        KafkaOffsetPersist kafkaOffsetPersist = KafkaTools.getKafkaOffsetPersist(inputParameterUtil);
        updateSaveType(inputParameterUtil);
        skContext.setJsc(jsc);
        skContext.setKafkaOffsetPersist(kafkaOffsetPersist);
        processor.load(jsc,kafkaOffsetPersist);
        JavaInputDStream<ConsumerRecord<String, String>> directStream = null;

        if(StringUtils.isNotEmpty(getCheckPoint()))
        {
            directStream = KafkaTools.getDirectStream(jsc,kafkaParams,kafkaOffsetPersist);
        }
        else
        {
            directStream = KafkaTools.getKafkaStream(jsc
                    ,kafkaParams
                    ,kafkaOffsetPersist);
        }
        skContext.setDirectStream(directStream);
        processor.process(directStream);
        if(StringUtils.isEmpty(getCheckPoint()))
        {
            KafkaTools.saveKafkaOffset(directStream,kafkaOffsetPersist);
        }
        else
        {
            KafkaTools.commitAsync(directStream);
        }
        processor.end();
        return this;
    }

    public void updateSaveType(InputParameterUtil inputParameterUtil)
    {
        String appName = inputParameterUtil.getAppName();
        Config config = ConfigUtil.getConfig();
        String saveType = config.getString("KafkaOffsetPersist");
        MySqlUtil.getInstance().ExecuteSQL("update Task set saveType='"+saveType+"' where appName='"+appName+"'");
    }

    public void start() throws InterruptedException {
        start(true);
    }

    public void start(Boolean stopByMark) throws InterruptedException {
        JavaStreamingContext jsc = skContext.getJsc();
        jsc.start();
        if(stopByMark)
        {
            StreamingUtil.stopByMark(skContext.getInputParameterUtil().getAppName(),jsc);
        }
        jsc.awaitTermination();
    }


}
