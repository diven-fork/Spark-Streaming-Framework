/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util.client;

import bigdata.java.framework.spark.pool.kafka.KafkaProducerConfig;
import bigdata.java.framework.spark.pool.kafka.KafkaProducerConnectionPool;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import java.util.List;
import java.util.Map;

/**
 * kafka生产者客户端工具类
 */
public class KafkaProducerClient {
    private static KafkaProducerClient instance=null;

    /**
     * 获取kafka客户端单实例对象
     */
    public static KafkaProducerClient getInstance(){
        if(instance == null){
            synchronized(KafkaProducerClient.class){
                if(instance==null){
                    instance = new KafkaProducerClient();
                }
            }
        }
        return instance;
    }
    KafkaProducerConnectionPool kafkaProducerConnectionPool ;
    private KafkaProducerClient()
    {
        KafkaProducerConfig kafkaProducerConfig = new KafkaProducerConfig();
        kafkaProducerConnectionPool = new KafkaProducerConnectionPool(kafkaProducerConfig);
    }

    /**
     * 获取连接池对象
     */
    public KafkaProducerConnectionPool getConnectionPool() {
        return kafkaProducerConnectionPool;
    }

    /**
     * 发送消息到kafka
     * @param topicName topic名称
     * @param strMessage 消息内容
     */
    public void sendMessage(String topicName, String strMessage) {
        KafkaProducerConnectionPool pool =null;
        KafkaProducer<String, String> resource =null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
//            System.out.println(resource);
            resource.send(new ProducerRecord<>(topicName, strMessage));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            if(pool!=null && resource!=null)
            {
                pool.returnConnection(resource);
            }
        }
    }

    /**
     * 发送消息到kafka(批量)
     * @param topicName topic名称
     * @param strMessages 消息内容(数组)
     */
    public void sendMessage(String topicName, String... strMessages) {
        KafkaProducerConnectionPool pool =null;
        KafkaProducer<String, String> resource =null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            for (String strMessage : strMessages) {
                resource.send(new ProducerRecord<>(topicName, strMessage));
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            if(pool!=null && resource!=null)
            {
                pool.returnConnection(resource);
            }
        }
    }
    /**
     * 发送消息到kafka(批量)
     * @param topicName topic名称
     * @param strMessages 消息内容(List数组)
     */
    public void sendMessage(String topicName, List<String> strMessages) {
        KafkaProducerConnectionPool pool =null;
        KafkaProducer<String, String> resource =null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            for (String strMessage : strMessages) {
                    resource.send(new ProducerRecord<String, String>(topicName, strMessage));
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            if(pool!=null && resource!=null)
            {
                pool.returnConnection(resource);
            }
        }
    }

    /**
     * 发送消息到kafka(批量)
     * @param topicName topic名称
     * @param strMessages 消息内容(List数组)
     * @param sendKey true包含key，false不包含key
     */
    public void sendMessage(String topicName, List<String[]> strMessages, Boolean sendKey) {
        KafkaProducerConnectionPool pool =null;
        KafkaProducer<String, String> resource =null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            for (String[] strMessage : strMessages) {
                if(sendKey)
                {
                    resource.send(new ProducerRecord<String, String>(topicName, strMessage[0],strMessage[1]));
                }
                else
                {
                    resource.send(new ProducerRecord<String, String>(topicName, strMessage[1]));
                }
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            if(pool!=null && resource!=null)
            {
                pool.returnConnection(resource);
            }
        }
    }

//    /**
//     * 发送消息到kafka(批量)
//     * @param topicName topic名称
//     * @param mapMessages 消息内容(List Map)
//     */
//    public void sendMessage(String topicName, List<Map<Object, Object>> mapMessages) {
//        KafkaProducerConnectionPool pool =null;
//        KafkaProducer<String, String> resource =null;
//        try
//        {
//            pool = getConnectionPool();
//            resource = pool.getConnection();
//            for (Map<Object, Object> mapMessage : mapMessages) {
//                String array = mapMessage.toString();
//                resource.send(new ProducerRecord<>(topicName, array));
//            }
//        }
//        catch (Exception e)
//        {
//            throw new RuntimeException(e);
//        }
//        finally {
//            if(pool!=null && resource!=null)
//            {
//                pool.returnConnection(resource);
//            }
//        }
//    }

    /**
     * 发送消息到kafka(批量)
     * @param topicName topic名称
     * @param mapMessage 消息内容(Map)
     */
    public void sendMessage(String topicName, Map<Object, Object> mapMessage) {
        KafkaProducerConnectionPool pool =null;
        KafkaProducer<String, String> resource =null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            String array = mapMessage.toString();
            resource.send(new ProducerRecord<String, String>(topicName, array));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            if(pool!=null && resource!=null)
            {
                pool.returnConnection(resource);
            }
        }
    }
}
