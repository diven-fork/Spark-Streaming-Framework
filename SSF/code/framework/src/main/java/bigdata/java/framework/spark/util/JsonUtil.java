/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * ogg kafka json工具类
 */
public class JsonUtil {

    /**
     * 新增
     */
    public static final String INSERT = "I";
    /**
     * 修改
     */
    public static final String UPDATE = "U";
    /**
     * 删除
     */
    public static final String DELETE = "D";

    /**
     * 自定义(一般适用于手动构建的消息)
     */
    public static final String CUSTOM = "C";

    /**
     * kafka ogg op_type
     */
    public static final String OP_TYPE = "op_type";

    /**
     * kafka ogg current_ts
     */
    public static final  String CURRENT_TS="current_ts";

    /**
     * kafka ogg pos
     */
    public static final  String POS="pos";

    /**
     *kafka ogg after
     */
    public static final String AFTER = "after";

    /**
     * kafka ogg op_ts
     */
    public static final  String OP_TS="op_ts";

    /**
     *kafka ogg before
     */
    public static final String BEFORE = "before";

    /**
     * 主键
     */
    public static final String PRIMARY_KEYS = "primary_keys";

    /**
     * ogg kafka参数
     */
    public static final String TABLE = "table";

    String json="";
    JSONObject jsonContent = null;
    JSONObject jsonObject = null;

    /**
     * 构造函数
     * @param json 传入json
     */
    public JsonUtil(String json)
    {
        this.json = json;
        jsonContent = getContent(json);
    }

    /**
     * 获取json中指定key的value值
     * @param key  指定的key
     * @return 没有找到返回空字符串
     */
    public String getString(String key)
    {
        if(jsonContent.get(key)!=null)
        {//客户编号
            return jsonContent.get(key).toString();
        }
        return "";
    }

    /**
     * 获取表名
     */
    public String getTable()
    {
        return this.jsonObject.getString(TABLE);
    }

    /**
     * 获取操作类型
     */
    public String getOp_Type()
    {
        return this.jsonObject.getString(OP_TYPE);
    }

    /**
     * 获取操作时间
     */
    public String getOp_Ts()
    {
        return this.jsonObject.getString(OP_TS);
    }

    /**
     * 获取当前时间
     */
    public String getCurrent_Ts()
    {
        return this.jsonObject.getString(CURRENT_TS);
    }

    /**
     * 获取ogg偏移量
     */
    public String getPos()
    {
        return this.jsonObject.getString(POS);
    }

    /**
     * 获取主键
     */
    public List<String> getPrimary_Keys()
    {//主键
        JSONArray array = this.jsonObject.getJSONArray(JsonUtil.PRIMARY_KEYS);
        if (array == null || array.size() < 1) {//没有主键
            return null;
        } else {
            List<String> keys = JSONArray.parseArray(array.toJSONString(), String.class);
            return keys;
        }
    }

    /**
     * 根据ogg kafka json获取JSONObject对象(根据ogg操作类型已经处理before和after)
     * @param json ogg kafka json 字符串
     */
    public JSONObject getContent(String json)
    {
        this.jsonObject = JSON.parseObject(json);
        String type = jsonObject.getString(OP_TYPE);
        if(type.toLowerCase().equals(DELETE.toLowerCase()))
        {
            return (JSONObject) jsonObject.get(JsonUtil.BEFORE);
        }
        else
        {
            return (JSONObject)jsonObject.get(JsonUtil.AFTER);
        }
    }

    /**
     * 根据key获取指定JSONObject对象的value
     * @param jsonObject jsonObject对象
     * @param key key
     * @return 没有找到返回空字符串
     */
    public static String getString(JSONObject jsonObject,String key)
    {
        if(jsonObject.get(key)!=null)
        {//客户编号
            return jsonObject.get(key).toString();
        }
        return "";
    }
}
