/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * oracle工具类(单库)
 * @deprecated 已废弃，建议使用bigdata.java.framework.spark.util.client.SqlClient
 */
@Deprecated
public class OracleUtil {

    private ComboPooledDataSource cpds;

    private static OracleUtil instance=null;

    private OracleUtil(){
        cpds = new ComboPooledDataSource("JdbcOracle");
    }

    /**
     * 获取工具类实例
     */
    public static OracleUtil getInstance(){
        if(instance == null){
            synchronized(OracleUtil.class){
                if(instance==null){
                    instance=new OracleUtil();
                }
            }
        }
        return instance;
    }

    /**
     * 获取数据库连接
     */
   public Connection getConnection(){
        Connection conn = null;
       try {
           conn = cpds.getConnection();
       } catch (SQLException e) {
           throw new RuntimeException(e);
       }
       return conn;
   }
    /**
     * 判断表是否存在
     * @param tableName 表名
     * @return true存在，false不存在
     */
   public Boolean tableExists(String tableName)
   {
       String sql = "select count(1) as c from user_tables where table_name = upper('"+tableName+"') ";
       Boolean tableExists = false;
       List<Map<String, Object>> list = QuerySQL(sql);
       for (int i = 0; i < list.size(); i++) {
           Object c = list.get(i).get("C");
           if("1".equals(c.toString()))
           {
               tableExists = true;
               break;
           }
       }
       return tableExists;
   }

    /**
     * ResultSet转换为 List
     * @param rs ResultSet结果集
     * @return map list对象
     */
    public List<Map<String, Object>> selectAll(ResultSet rs) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        if(rs==null)
        {
            return list;
        }
        try {
            // 获取结果集结构（元素据）
            ResultSetMetaData rmd = rs.getMetaData();
            // 获取字段数（即每条记录有多少个字段）
            int columnCount = rmd.getColumnCount();
            while (rs.next()) {
                // 保存记录中的每个<字段名-字段值>
                Map<String, Object> rowData = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; ++i) {
                    // <字段名-字段值>
                    rowData.put(rmd.getColumnName(i), rs.getObject(i));
                }
                // 获取到了一条记录，放入list
                list.add(rowData);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    /**
     * 执行查询sql语句
     * @param sql sql语句
     * @return 返回结果集list
     */
    public List<Map<String, Object>> QuerySQL(String sql)
    {
        // 定义结果集
        ResultSet rs = null;
        Connection connection = null; // 连接
        PreparedStatement pstm  = null; //
        List<Map<String, Object>> maps = new ArrayList<>();
        try{
            connection = getConnection();
            // 执行SQL 语句, 返回结果集
            pstm = connection.prepareStatement(sql);
            rs   = pstm.executeQuery();
            maps = selectAll(rs);
        } catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            if(rs!=null)
            {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(pstm!=null)
            {
                try {
                    pstm.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return maps;
    }

    /**
     * 批量执行sql （带事物）
     * @param list 批量sql语句list
     */
    public Boolean ExecuteSqlList(List<String> list)
    {
        Connection connection = null; // 连接
        PreparedStatement pstm  = null; //
        Boolean success=false;
        try{
            connection = OracleUtil.getInstance().getConnection();
            connection.setAutoCommit(false);

            for (int i = 0; i < list.size(); i++) {
                String sql = list.get(i);
                pstm = connection.prepareStatement(sql);
                pstm.executeUpdate();
            }
            connection.commit();
            success = true;
        } catch (SQLException e){
            try {
                if(connection!=null)
                {
                    connection.rollback();
                }
            } catch (SQLException e1) {
                throw new RuntimeException(e1);
            }
            throw new RuntimeException(e);
        } finally{
            try {
                if(pstm!=null)
                {
                    pstm.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                if(connection!=null)
                {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return success;
    }

    /**
     * 执行增，删，改sql语句
     * @param sql 执行sql语句
     * @return 返回影响的行数
     */
    public int ExecuteSQL(String sql)
    {
        Connection connection = null; // 连接
        PreparedStatement pstm  = null; //
        int code = 0;
        try{
            connection = getConnection();
            pstm = connection.prepareStatement(sql);
            code = pstm.executeUpdate();
//            connection.commit();
        } catch (SQLException e){
            throw new RuntimeException(e);
        } finally{
            try {
                if(pstm!=null)
                {
                    pstm.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                if(connection!=null)
                {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return code;
    }

    /**
     * 关闭连接
     * @param con Connection对象
     * @param preparedStatement PreparedStatement对象
     * @param rs ResultSet对象
     */
   public static void close(Connection con , PreparedStatement preparedStatement , ResultSet rs){
          if(null != rs){
              try {
                  rs.close();
              } catch (SQLException e) {
                  throw new RuntimeException(e);
              }
          }

          if(null != con){
              try {
                  rs.close();
              } catch (SQLException e) {
                  throw new RuntimeException(e);
              }
          }

          if (null != preparedStatement){
              try {
                  preparedStatement.close();
              } catch (SQLException e) {
                  throw new RuntimeException(e);
              }
          }
   }
}
