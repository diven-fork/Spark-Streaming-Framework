/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.sql;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.Connection;

public class SqlConnectionPool {

    private ComboPooledDataSource cpds;
    SqlConfig mysqlConfig;
    public SqlConnectionPool(SqlConfig mysqlConfig)
    {
        this.mysqlConfig = mysqlConfig;
        initPool();
    }

    void initPool()
    {
        cpds = new ComboPooledDataSource(mysqlConfig.getConfigName());
    }

    public ComboPooledDataSource getCpds() {
        return cpds;
    }

    public Connection getConnection() {
        Connection connection =null;
        try
        {
            connection = cpds.getConnection();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return connection;
    }

    public void returnConnection(Connection connection) {
        try
        {
            connection.close();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
