package bigdata.java.platform.core;


import bigdata.java.platform.beans.UserInfo;
import bigdata.java.platform.util.Comm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Component
public class SecurityInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        String method = request.getMethod();
        String contextPath = request.getContextPath();
        System.out.println("getMethod:"+method+",contextPath:"+contextPath+"，SecurityInterceptor:"+uri);
        // 验证权限
        if (this.hasPermission(handler,request,response)) {
            return true;
        }
        return false;
    }

    private boolean hasPermission(Object handler,HttpServletRequest request,HttpServletResponse response) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            // 获取方法上的注解
            Permission permission = handlerMethod.getMethod().getAnnotation(Permission.class);
            // 如果方法上的注解为空 则获取类的注解
            if (permission == null) {
                permission = handlerMethod.getMethod().getDeclaringClass().getAnnotation(Permission.class);
            }
            HttpSession session = request.getSession();
            UserInfo user = (UserInfo) session.getAttribute("userinfo");
            String contextPath = request.getContextPath();
            if (permission != null && user==null) {//如果标记了注解验证权限,但是没有登录的，自动跳到登录页面
                response.sendRedirect(contextPath+"/login");
                return false;
            }
//            // 如果标记了注解，但是没有设置权限，value="",那么只需要判断是否登录即可
//            if (permission != null && !StringUtils.isNotBlank(permission.value())) {
//                if (user == null) {
//                    response.sendRedirect(contextPath+"/manage/user/login");
//                    return false;
//                }
//            }
            // 如果标记了注解，则判断权限
            if (permission != null && StringUtils.isNotBlank(permission.value())) {

                boolean checked = Comm.checkAuthority(permission.value());
                if(!checked)
                {//没有权限，跳转到 无权限显示页面
                    request.getRequestDispatcher("/nopermission").forward(request,response);
                }
                return checked;
            }
        }
        return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
