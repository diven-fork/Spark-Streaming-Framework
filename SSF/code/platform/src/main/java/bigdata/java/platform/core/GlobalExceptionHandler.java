package bigdata.java.platform.core;

import bigdata.java.platform.beans.Resoult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
    // 日志记录工具
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @ExceptionHandler(value = Exception.class)
    public Object handleGlobalException(HttpServletRequest request, Exception ex) {

        String requestURI = request.getRequestURI();
        String httpMethod = request.getMethod();

        Map<String,Object> map = new HashMap<String,Object>();
        Enumeration paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();
            String[] paramValues = request.getParameterValues(paramName);
            if (paramValues.length >0) {
                String paramValue = paramValues[0];
                if (paramValue.length() != 0) {
                    map.put(paramName, paramValue);
                }
            }
        }
        String requestParam="";
        Set<Map.Entry<String, Object>> set = map.entrySet();

        for (Map.Entry entry : set) {
            requestParam+="\t"+entry.getKey() + ":" + entry.getValue()+"\n";
        }

        //打印堆栈日志到日志文件中
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        ex.printStackTrace(new java.io.PrintWriter(buf, true));
        String  expMessage = buf.toString();
        try {
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //记录到日志
        log.error("GlobalExceptionHandler:\n"+"request url:\n\t["+httpMethod+"] "+requestURI+"\nrequest parameter:\n"+requestParam+ ex.getMessage() + ";Exception:" + expMessage);

        Object resoult = null;
        if(requestURI.contains("/api"))
        {//api发生错误
            resoult = Resoult.fail();
        }
        else
        {//manage
            ModelAndView mv = new ModelAndView();//404错误不需要处理，springboot会自动处理，只要在error目录下创建404.html文件即可
            mv.setViewName("/error/error");
            resoult = mv;
        }

        return resoult;
    }
}
