package bigdata.java.platform.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TTask {

    Integer taskId;
    String taskName;
    String appName;
    String classFullName;
    Integer status;
    Integer errCount;

    public String getSaveType() {
        return saveType;
    }

    public void setSaveType(String saveType) {
        this.saveType = saveType;
    }

    String saveType;

    Integer tryCount;

    Date startTime;

    String queueName;

    String webApi;

    String openChart;

    public String getOpenChart() {
        return openChart;
    }

    public void setOpenChart(String openChart) {
        this.openChart = openChart;
    }

    public String getWebApi() {
        return webApi;
    }

    public void setWebApi(String webApi) {
        this.webApi = webApi;
    }

    public String getWebApiToken() {
        return webApiToken;
    }

    public void setWebApiToken(String webApiToken) {
        this.webApiToken = webApiToken;
    }

    String webApiToken;

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Integer getTryCount() {
        return tryCount;
    }

    public void setTryCount(Integer tryCount) {
        this.tryCount = tryCount;
    }

    public String getWarnPhone() {
        return warnPhone;
    }

    public void setWarnPhone(String warnPhone) {
        this.warnPhone = warnPhone;
    }

    String warnPhone;

    public Integer getErrCount() {
        return errCount;
    }

    public void setErrCount(Integer errCount) {
        this.errCount = errCount;
    }

    TaskLog taskLog  = null;

    public TaskLog getTaskLog() {
        return taskLog;
    }

    public void setTaskLog(TaskLog taskLog) {
        this.taskLog = taskLog;
    }

    public static final Integer STARTED  = 20;//启动成功
    public static final Integer STARTING  = 21;//启动中
    public static final Integer LIVYSTARTING = 22;//Livy提交中
    public static final Integer STOPED  = 10;//停止成功
    public static final Integer STOPING  = 11;//停止中
    public static final Integer DELETE  = 00;//删除
    public static final Integer ERROR  = 99;//错误

    public String getStatusText() {
        //10停止
        //11停止中
        //20启动
        //21启动中
        if(status.intValue() ==10)
        {
            return "停止";
        }
        else if(status.intValue()==11)
        {
            return "停止中";
        }
        else if(status.intValue()==20)
        {
            return "启动";
        }
        else if(status.intValue()==21)
        {
            return "启动中";
        }
        else if(status.intValue()==22)
        {
            return "Livy提交中";
        }
        else
        {
            return "未知";
        }
    }

    Date updateTime;
    String conf;
    String args;
    String jars;
    String files;
    String param;
    Integer jobId;
    Integer userId;
    Integer taskType;

    String activeBatch;
    String restartActiveBatch;

    public String getActiveBatch() {
        return activeBatch;
    }

    public void setActiveBatch(String activeBatch) {
        this.activeBatch = activeBatch;
    }

    public String getRestartActiveBatch() {
        return restartActiveBatch;
    }

    public void setRestartActiveBatch(String restartActiveBatch) {
        this.restartActiveBatch = restartActiveBatch;
    }

    public String getOpenRestartActiveBatch() {
        return openRestartActiveBatch;
    }

    public void setOpenRestartActiveBatch(String openRestartActiveBatch) {
        this.openRestartActiveBatch = openRestartActiveBatch;
    }

    String openRestartActiveBatch;

    public SparkJob getJob()
    {
        for (int i = 0; i < getJobList().size(); i++) {
            SparkJob job = getJobList().get(i);
            if(job.getJobId().intValue() == getJobId().intValue())
            {
                return job;
            }
        }
        return null;
    }


    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    List<SparkJob>  jobList = new ArrayList<>();

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }
    public List<SparkJob> getJobList() {
        return jobList;
    }
    public void setJobList(List<SparkJob> jobList) {
        this.jobList = jobList;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getClassFullName() {
        return classFullName;
    }

    public void setClassFullName(String classFullName) {
        this.classFullName = classFullName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getJars() {
        return jars;
    }

    public void setJars(String jars) {
        this.jars = jars;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
