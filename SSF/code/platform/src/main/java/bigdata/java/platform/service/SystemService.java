package bigdata.java.platform.service;

import bigdata.java.platform.beans.SysSet;

public interface SystemService {
    SysSet get(Integer userId);
    Boolean edit(SysSet sysSet);
    Boolean add(SysSet sysSet);
}
